#!/bin/bash

#####################################################################################
# This script is used to push code and other artifacts to S3. 
#
# The script is dependent on the various resources getting deployed using the cloudformation
# template file: cloudformation/datasync_template.yaml. The resource and their information
# will be extracted once the stack is deployed successfully.
#
# Example :
# bin/step_1_1_push_code_artifacts_s3.sh "glue-dsync-base"
#####################################################################################

PARAM_DSYNC_BASE_STACKNAME=${1:-'glue-dsync-base'} #The cloudformation stack name

PROJECT_DIR="${PWD}"
BUILD_DIR="${PROJECT_DIR}/build"


## ---------------------------------------------------------------------------------------- ##
# # Retreive the url & arn based of the export from the deployed cloud formation template (dbtonfargate.yml)
deployed_env=`aws cloudformation describe-stacks --stack-name "${PARAM_DSYNC_BASE_STACKNAME}" --query "Stacks[].Parameters[?ParameterKey=='TAGEnv'].ParameterValue" --output text` #ex: dev
echo "Stack[${PARAM_DSYNC_BASE_STACKNAME}] environment : $deployed_env"

# # Retreive the url & arn based of the export from the deployed cloud formation template (dbtonfargate.yml)
s3_codebucket_url_export_name="${PARAM_DSYNC_BASE_STACKNAME}-${deployed_env}-CodeStageBKTS3URL" #ex: glue-dsync-base-dev-CodeStageBKTS3URL

s3_bucket_url=`aws cloudformation list-exports --query "Exports[?Name == '${s3_codebucket_url_export_name}'].Value" --output text`
echo "s3 bucket url : $s3_bucket_url"

## ---------------------------------------------------------------------------------------- ##

code_folder=${s3_bucket_url}/code/glew

echo "Uploading to ${code_folder} ..."
aws s3 cp glew/dsync.py ${code_folder}/dsync/
aws s3 cp glew/aws_stepfn.json ${code_folder}/dsync/

echo "Clean up previous build ..."
rm -rf ${BUILD_DIR}
mkdir -p ${BUILD_DIR}

wget -P ${BUILD_DIR}/ https://repo1.maven.org/maven2/net/snowflake/snowflake-jdbc/3.12.8/snowflake-jdbc-3.12.8.jar
wget -P ${BUILD_DIR}/ https://repo1.maven.org/maven2/net/snowflake/spark-snowflake_2.11/2.7.2-spark_2.4/spark-snowflake_2.11-2.7.2-spark_2.4.jar
wget -P ${BUILD_DIR}/ https://repo1.maven.org/maven2/com/microsoft/sqlserver/mssql-jdbc/7.4.1.jre8/mssql-jdbc-7.4.1.jre8.jar
echo "Uploading libraries ..."
lib_folder=${s3_bucket_url}/code/glew/lib/
aws s3 cp "${BUILD_DIR}" "${lib_folder}" --recursive
