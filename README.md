# DSYNCWITHGLUE : Data sychronization with AWS Glue
A micro reference implementation of performing data synchronization from popular database data into Snowflake using AWS Glue

- Date: 1-July-2020
- Source Code: [https://gitlab.com/snowflakeproto/dsyncwithglue](https://gitlab.com/snowflakeproto/dsyncwithglue)

[[_TOC_]]

## Overview
It has been a consistent scenario, in my various client engagements, that clients look for cost effective solution to synchronize
data with Snowflake. Though clients might have bought products like Matilion, Fivetran, Attunity etc..; client are always looking
for option B. While these vendor products offer easier integrations, simplicity, quick up and run time but thier licensing cost 
eats up budget thats been allocated to data teams. 

Another reason would be that the data team bought a product ex: Attunity, they have a need that requires data synchronization from
a source that is current being developed/not supported by Attunity. In this case, going out and acquiring Stitch like solution 
would a longer time to get approvals, license etc..

### Option B : Alternatives
Some open source for data synchronization do exist:
- [https://www.symmetricds.org/](https://www.symmetricds.org/) : This however requires changes to source database, which not all source
 sytems or external vendor source database will allow.
 - [Matillion Data Loader](https://www.matillion.com/products/data-loader/) : Quick service however the code operates from Matilion environment, also you 
 would have to open firewall ports for Matillion to connect. Not all clients would be willing to proceed with this.
 - [https://www.stitchdata.com/](https://www.stitchdata.com/pricing/) : Stich offers a free plan, again code runs in Stitch's environment,
 not all clients would be willing to open unless contracts are signed etc..

### Option B: Cloud native solutions
The option B can be cloud native solutions like Azure Data Factory (ADF), AWS Glue or Google data flow. While these cloud native 
solutions would not give everything that vendor products like Fivetran, Stitch etc.. offer. But the following perspective instead of
developing from scratch in house
- Documentation
- No need to wait for long sales negotiation/licensing timeline.
- Managed Service (Less administration)
- Horizontally scallable.
- Always evolving, almost every six months with newer capabilities and enhancements.
- Easily integrated with native cloud security services like Azure AD, AWS IAM
- Integrated with cloud native IAC kits like Azure ARM Templates, AWS Cloudformation etc..
- On-demand pricing
- Integrated with cloud native logging/monitoring/billing modules.

While these cloud native solution might not have everything that you have, but they typically offer flexibility in developing custom
implementations if the base products dont have it. For example for ADF you would develop Azure Function that can integrate with your
custom data source, In AWS Glue you can call a lambda/spark data source that integrates with your data source.

> "AWS Glue is a fully managed extract, transform, and load (ETL) service that makes it easy for  customers to prepare and load their 
data" - [AWS Glue](https://aws.amazon.com/glue).

> "With AWS Glue, you only pay for the time your ETL job takes to run. There are no resources to manage, no upfront costs, and you are not charged for startup or shutdown time. You are charged an hourly rate based on the number of Data Processing Units (or DPUs) used to run your ETL job." - [Glue pricing](https://aws.amazon.com/glue/pricing/).

In this project, I am providing a base implementation for performing data synchronization with an RDBMS source (SQLServer) to ingest 
data into Snowflake, using AWS Glue. There are various blogs and presentations on using AWS Glue for data loading, however these are
related to S3/Redshift. I did not find any implementation specific to Snowflake, hence decided to share the knowledge and implementation
with the public. 

While i would not be going over the basic technology information, i am hoping that you are aware of the various components that are 
used. You would need to be aware of the following tech stacks:
- AWS Glue - Job
- AWS Cloudformation
- AWS IAM
- AWS Step function
- AWS Secrets manager

In order to adopt AWS Glue, I would recommend that it be developed by a Spark developer. The marketting presentations does not imply
this directly, but it comes very apparent as soon as you develop and start investigating failures in log. 

What i will be presenting:
- A hypotical scenario
- Approach & Implementation code

## Scenario
It is a common scenario that we need to load data into Snowflake from an RDBMS. Hence I will be performing a loading data from a AWS RDS (SQL-Server)
instance into Snowflake. I choose AWS RDS for easier up and running; it could be any RDBMS running anywhere, as long as AWS Glue can access it. The below image depicts the solution 
overview:

![](doc/images/dsync_snowflake_with_glue.jpg)

- The scenario would need to handle initial and delta data load. 
- The data should not be stored in intermediate data store, like S3 (I have seen occurence of this type of scenario). S3 can hold the data only for a temporary basis, only for the duration of the data loading process.
- The delta data load are any records that are any records that are added or updated since the last data load. 
- Hard delete of record is not an issue.
- Data is to be stored in Snowflake in an AS-IS state. All transformation would be handled in Snowflake.

### Solution overview

- **Source Code:** [https://gitlab.com/snowflakeproto/dsyncwithglue](https://gitlab.com/snowflakeproto/dsyncwithglue) __

A standard typical approach would be to  perform a meta-data crawl on the source database followed by a dataload into a target data store. We would be using AWS Glue purely from the perspective of being a managed service and various core functionalities that it offers. AWS Glue essentially runs a managed Apache Spark, you would implement the code as you would do with any other programs. Since the main focus is just to transfer data from the source db to Snowflake, no transformation or any processing will be done; hence the process/code itself would be light. AWS Glue dynaframe classes will also not be used, will explain why in later sections.

A schedule time trigger starts up the step function. Each "Task" in the step function represents a glue job invocation. The glue job perform identify and synch the records.  
![](doc/images/stepfunctions_graph.png)

The glue job implements the following logic:

```mermaid
graph TD
    A[[Connect to Snowflake ]]
    B{Does table exist?}
    C[[Perform full data extract]]
    D[[Find the last record synched]]
    E[[Extract record from source]]
    F[[Ingest record into Snowflake]]
    
    A-->B;
    B-- N -->C;
    C-->E;
    E-->F;
    B-- Y -->D;
    D-->E;
```

Instead of hard coding the queries, we will parameterize the various queries and SQL instruction. During run time, via parameter override, we will be able to use a single glue job definition for multiple table.

Please go over the [DesignOpinions](doc/DesignOpinions.md) document, as it covers some aspects and areas when developing this solution.

#### Implementation Code

##### Glue Job logic
- ** Code :** [glew/dsync.py](glew/dsync.py)

The job has been developed using PySpark and has been developed as a base minimal code. The logic of synchronizing the data has been explained above. The way of
connecting to source DB and Snowflake is via driver jars. Connection settings are retrieved from the secrets manager. The following are the job parameters:

| Parameter | Description | Example |
|--|--|--|--|
| SFLK_CONN_SECRETS | The arn of secrets manager holding the snowflake connection information. | arn:aws:secretsmanager:us-east-2:999999:secret:glue-dsync-base/dev/SFLKConn-CMfC4A |
| SOURCEDB_CONN_SECRETS | The arn of secrets manager holding the source db connection information. | arn:aws:secretsmanager:us-east-2:99999:secret:glue-dsync-base/dev/SourceDB-EUpvJD |
| DSYNC_TABLE | The table that needs to be synched as stored in Snowflake target. I recomend the full path to the table, meaning <database name>.<schema>.<target table> | TEST_DB.VENKAT.PERSON |
| TABLE_EXIST_QUERY | Query executed against Snowflake to verify if the target table exists. | select count(0) cnt from TEST_DB.INFORMATION_SCHEMA.TABLES where TABLE_CATALOG='TEST_DB' and TABLE_SCHEMA='VENKAT' and TABLE_TYPE='BASE TABLE' and TABLE_NAME='PERSON' |
| LATEST_WATERMARK_QUERY | Query to find the watermark from the Snowflake table. This will be called only if the table exist. | select TO_VARCHAR(max(modifieddate) ,'YYYY-MM-DD HH24:MI:SS.FF3') from TEST_DB.VENKAT.PERSON |
| INITIAL_LOAD_QUERY | Query issued to Source DB to do a full data extract. Issued in case the target table in Snowflake does not exist. | (select * from adventureworks.Person.Person) AS datatbl |
| DELTA_LOAD_QUERY | Query issued to Source DB to retrieve records that have been inserted/updated since last data refresh. The '{}' is a placeholder where the watermark value will be substituted. |  (select * from adventureworks.Person.Person where modifieddate > '{}') AS datatbl |

Once a Job is executed, you can find the related logs & status in the "History" tab in the glue console:

![](doc/images/job_execution.jpg)

##### Step function
- ** Code :** [glew/aws_stepfn.json](glew/aws_stepfn.json)

The current implementation will synch only one table at a time. In a typical implementation multiple tables would need to be synched, to co-ordinate/orchesterate
this I have adopted Step Function. In the current implementation, this functionality has been done where we synch table in sequential manner, you can change this to parallel execution in the step function, instead of modifying the Glue Job code. However, be vary of the load that would occur on the Source DB.

As mentioned earlier, instead of creating job definition for each table, we create only one job definition. The table to be synched is determined by the Job Parameters,which we override during execution. In the below example, I am loading the "Person" and "StateProvince" tables.

![](doc/images/step_fn_definition.jpg)

##### S3
The code will be hosted in S3 buckets. The following folder hierarchy is expected in the code:
 - code/glew/lib : Holds libraries like JDBC drivers
 - code/glew/dsync : Holds Glue code and step function definition

##### Cloudformation templates

- [cloudformation/datasync_template.yaml](cloudformation/datasync_template.yaml) : Creates the core stack resources for this solution. Resource like S3 bucket, IAM Role are defined here. Please refer to various embedded descriptions.
- [cloudformation/datasync_jobtemplate.yaml](cloudformation/datasync_jobtemplate.yaml) : Creates the job and step function definition. Please refer to various embedded descriptions.

![](doc/images/glue-dsync-base.jpg)

#### Deployment

Refer : [Deployments](doc/Deployments.md)

#### Sample Execution

Here is a sample execution of invoking the data synchronization, by invoking the
Step function manually in the console :

![](doc/images/step_fn_execution.jpg)

##### Invoke via AWS CLI
You can also invoke the glue job using AWS CLI as below. The job has been defined as 'glue-dsync-base-dev-DsyncJob'. The sample demonstrated synchronizing the 'Person' table.

1) Define a parameter file ex: data_load_config.json .The content is as below:
```json
  {
  "JobName": "glue-dsync-base-dev-DsyncJob",
  "Arguments": {
    "--DSYNC_TABLE" : "TEST_DB.VENKAT.PERSON",
    "--TABLE_EXIST_QUERY" : "select count(0) cnt from TEST_DB.INFORMATION_SCHEMA.TABLES where TABLE_CATALOG='TEST_DB' and TABLE_SCHEMA='VENKAT' and TABLE_TYPE='BASE TABLE' and TABLE_NAME='PERSON' ",
    "--INITIAL_LOAD_QUERY" : "(select * from adventureworks.Person.Person) AS datatbl",
    "--LATEST_WATERMARK_QUERY" : "select TO_VARCHAR(max(modifieddate) ,'YYYY-MM-DD HH24:MI:SS.FF3') from TEST_DB.VENKAT.PERSON", 
    "--DELTA_LOAD_QUERY" : "(select * from adventureworks.Person.Person where modifieddate > '{}') AS datatbl"

  }
}
```

2) Invoke :
```shell
  aws glue start-job-run --job-name glue-dsync-base-dev-DsyncJob --cli-input-json file://data_load_config.json 
```

## Additional Documentations:
 - [DesignOpinions](doc/DesignOpinions.md)
 - [Technical Debts](doc/Technical_debts.md)
 - [Deployments](doc/Deployments.md)

## Links
  - https://aws.amazon.com/blogs/big-data/best-practices-to-scale-apache-spark-jobs-and-partition-data-with-aws-glue/
  - [https://support.snowflake.net/s/article/How-To-Use-AWS-Glue-With-Snowflake](https://support.snowflake.net/s/article/How-To-Use-AWS-Glue-With-Snowflake)
  - https://medium.com/@joaopedro.pinheiro88/how-to-increase-data-load-speed-from-database-with-pyspark-3741cccdf928
  - https://docs.aws.amazon.com/glue/latest/dg/monitor-glue.html
  - https://aws.amazon.com/blogs/big-data/load-data-incrementally-and-optimized-parquet-writer-with-aws-glue/
