# Design Opinion

## Source Database.
I will not be explaining much on the RDS instance, as this could be any database. For doing this implementation I created a simple micro instance of SQL Server.
It was loaded with the "Adventure Works" sample dataset, as explained in this [link](https://www.mssqltips.com/sqlservertip/5970/restore-sql-server-database-backup-to-an-aws-rds-instance-of-sql-server/).

![](doc/images/rds.jpg)

We will be connecting to the RDS using JDBC. Appropriate security groups were also defined that allowed connection from the internet. When loading from an on-prem database, a direct connect would be preferrable vs over the public internet.s

## AWS Glue
  As mentioned earlier, we would be using Glue especially the managed perspective and per second billing. I choose Glue over EMR as it would require better and deeper understanding of Hadoop and other functionalities. In my current client engagements, not all clients resources are experienced in Hadoop tech stack. Hence to keep it simple Glue was choosen. Another option of using Databricks would also be a choice, but since the process itself being simple going to Databricks would not offer much value.

  ![](doc/images/glue_job_defn.jpg)

#### JDBC Driver & DynaFrame's
  The job would connect to the source database using the RDBMS specific JDBC driver. The driver library will be hosted in an S3 bucket and this will be reffered in the job 
  parameter "extra-jars". For connecting to Snowflake we would adopt the same approach, this article, ["HOW TO USE AWS GLUE WITH SNOWFLAKE"](https://support.snowflake.net/s/article/How-To-Use-AWS-Glue-With-Snowflake), gives a good overview of this approach.

![](doc/images/s3_code_lib_job_assoc.jpg)

 When connecting to the source database, we dont use the AWS Glue's class Dynaframe as we are mainly loading structured dataset. Moreover Dynaframe needs the metadata to be 
 populated in the AWS Data catalog, which is another step involved. Issuing SQL queries on the Dynaframe is also not supported. Hence to keep it simple, we directly connect 
 to source using jdbc driver with specific sql queries, which identifies the set of records that needs to be extracted. 

#### Snowflake Libraries and Glue version compatability
  One issue that I had struggled early on is that recent version of Glue does not it will work with the latest and greatest Snowflake JDBC & Spark jars. This was evident via this [Forum Post](https://support.snowflake.net/s/question/0D50Z00009UvjMVSAZ/aws-glue-fails-to-run-the-snowflake-python-connection-as-in-example-httpswwwsnowflakecombloghowtouseawsgluewithsnowflake).

  In the code, i might be using an older version but it works with Glue version 1.0. 

#### Job Bookmark & Identifying new/updated records
 AWS Glue offers [Job Bookmark](https://docs.aws.amazon.com/glue/latest/dg/monitor-continuations.html) as a mean of performing only delta data load. However as of today for JDBC 
 sources the support is limited to incremental columns.

 ![](doc/images/glue_jdbc_notes.jpg)

 Well not all data synch can adopt this strategy. In my experience a typical scenario is sources updating records in table in place. In these cases the record id remains the same 
 the change is identified by a field such as "modified date" or "last updated timestamp" or "last updated date". Hence the Job Bookmark functionality will not be adopted.

 So to overcome this, we query the table in Snowflake to find the last known watermark (ex: Max of Modified date) and retreive the records from the source which has greater than this
 marker.Since the table and the implementation can change from one scenario to another, we allow the implementator to dictate the query to check and retrieve watermark from the Snowflake 
 table and also to query the delta records from the source database by using [Job Parameters](https://docs.aws.amazon.com/glue/latest/dg/aws-glue-api-crawler-pyspark-extensions-get-resolved-options.html).

#### Table create/update
  It is not mandatory to create the table in Snowflake ahead of time. Spark write would take care of creating/appending the table as necessary.

#### Running in Private Subnet
  When Glue is executing as part of a private Subnet, it would not be able to connect to external sites like Snowflake. The errors that are thrown can be misleading too.

  The reason behind this is explained in this article ["Connect to and run ETL jobs across multiple VPCs using a dedicated AWS Glue VPC"](https://aws.amazon.com/blogs/big-data/connecting-to-and-running-etl-jobs-across-multiple-vpcs-using-a-dedicated-aws-glue-vpc/) :
  > AWS Glue resources are created with private addresses only. Thus, they can’t use an internet gateway to communicate with public addresses, such as public database endpoints. If your database endpoints are public, you can alternatively use a network address translation (NAT) gateway with AWS Glue rather than peer across VPCs.". 
  
  The article does walk-thru on how to setup and configure the NAT and security group rules, which allow this communication to happen. I have tested this out and confirm it working. 
  
  **Note:** In this presented implementation, glue is running part of a public subnet.Hence you would not find the configurations related to NAT & security groups.

## AWS Secrets Manager
 Following best practices, we store the connection information in AWS Secrets Manager.

 ![](doc/images/secrets_config.jpg)

## IAM Role
 I have defined a custom IAM role 'GlueJobExecRole' with the following rules:
  - Allow access to S3 code bucket which holds the code.
  - Allow read the secrets manager which has connection information for the source database and snowflake.
  - Allow step function to invoke glew job

 This has been defined in the cloudformation template [cloudformation/datasync_template.yaml](cloudformation/datasync_template.yaml).

 